#!/bin/sh

GODOT=/opt/Godot_v3.2.1-stable_x11.64
GAME=Adrenaline_rush

EXPORT_DIR=Exports

mkdir -p $EXPORT_DIR/$GAME.lin/
mkdir -p $EXPORT_DIR/$GAME.HTML/
#mkdir -p $EXPORT_DIR/$GAME.mac/
mkdir -p $EXPORT_DIR/$GAME.win/

$GODOT --export "Linux/X11" $EXPORT_DIR/$GAME.lin/$GAME.x86_64
$GODOT --export "HTML5" $EXPORT_DIR/$GAME.HTML/$GAME.html
# $GODOT --export "Mac OSX" $EXPORT_DIR/$GAME.mac.zip
$GODOT --export "Windows Desktop" $EXPORT_DIR/$GAME.win/$GAME.exe

mv $EXPORT_DIR/$GAME.HTML/$GAME.html $EXPORT_DIR/$GAME.HTML/index.html

cd $EXPORT_DIR

rm *.zip

zip -r $GAME.lin.zip $GAME.lin/*
zip -r $GAME.win.zip $GAME.win/*

# Move to dir so index.html is at the zip's root
cd $GAME.HTML
zip -r $GAME.HTML.zip ./*
mv $GAME.HTML.zip ..
