![baniere](baniere.png)

_Adrenaline Rush_ is a tiny game made during [Ludum Dare
46](https://ldjam.com/events/ludum-dare/46/$198809). This is a series
of mini-games centered around saving lives! The theme of the game jam
was "Keep it Alive", so the action takes place in an ambulance,
everything goes fast and you have to help your patients! Stabilizing
them! Well, the protagonist may be a lil' extreme in his actions, but
if it means saving lives, it's totally worth it!

### Controls:
Each mini-game uses the mouse. controls are explained right before it
starts.

The concept of the game was heavily inspired by Warioware: mini-games
in quick succession, simple commands and over-the-top action mixed
with ridiculous situations and exquisite humour.

Due to our tight schedule, we decided to only make 2 mini-games but at
least the concept is there! And it was fun to make, which is the
important part here!

Made by: Damien Picard, Duy Kevin Nguyen, Floriane Nguyen, Julien
Delwaulle, Clément Delwaulle.

### Licence
- Code is available under the terms of MIT (expat) licence
  (https://opensource.org/licenses/MIT​)
- Music and art are available under the terms of ​Creative Commons
  Attribution-ShareAlike 4.0 International​​
  (https://creativecommons.org/licenses/by-sa/4.0/)

Licence text files are available in the source.


![capture](ouais.png)

![capture](electro.png)
