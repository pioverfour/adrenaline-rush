shader_type canvas_item;

uniform sampler2D mask;
uniform vec4 mask_color : hint_color;
uniform float mask_scale = 0;
uniform float mask_alpha_scale = 0;
uniform vec2 offset;

void fragment() {
	vec2 uv = UV;
    vec4 bg = texture(TEXTURE, UV);
    vec4 m = texture(mask, (uv - offset) / mask_scale + offset).rgba;
    m.rgb = bg.rgb;
    float mask_alpha = texture(mask, (uv - offset) / mask_alpha_scale + offset).a;
    m.a = m.a * (1.0 - mask_alpha);

	COLOR.rgba = m;
}