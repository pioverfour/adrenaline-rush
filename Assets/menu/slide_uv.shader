shader_type canvas_item;

uniform vec2 offset_speed;

void fragment() {
	vec2 uv = UV;
    vec4 colour = texture(TEXTURE, uv + offset_speed * TIME);

	COLOR.rgba = colour;
}
