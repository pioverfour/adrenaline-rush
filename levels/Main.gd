extends Node2D

enum {MAIN, MENU, DEFIBRILATEUR, ADRENALINE, END}
var current_game = MENU

onready var scene_menu = $menu
onready var scene_defibrilateur = $Defibrilateur
onready var scene_adrenaline = $Adrenaline
onready var scene_end = $end
onready var transition = $camera/CanvasLayer/transition

export var level_number = 4
var level_index = 0

#var cooldown = Timer.new()

func _ready():
    Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
    scene_menu.visible = true
    scene_adrenaline.stop()
    scene_defibrilateur.stop()
    scene_end.visible = false
    transition.visible = false
    
    $music/menu.playing = true
    $music/camion.playing = false
    
#    add_child(cooldown)
#    cooldown.connect("timeout", self, "switch_scene")


# func _input(event):
#    if (event is InputEventKey
#            and event.scancode == KEY_SPACE):
#        scene_defibrilateur.visible = false
#        scene_defibrilateur.stop()
#        scene_adrenaline.visible = true
#    if (event is InputEventKey
#            and event.scancode == KEY_ENTER):
#        scene_defibrilateur.visible = true
#        scene_defibrilateur.init()
#        scene_adrenaline.visible = false
#     if (event is InputEventKey
#             and event.scancode == KEY_SPACE):
#         OS.max_window_size = Vector2(1920, 1080)
#         OS.window_size = Vector2(1920, 1080)
#         # Retrieve the captured Image using get_data().
#         var img = get_viewport().get_texture().get_data()
#         # Flip on the y axis.
#         img.flip_y()
#         img.save_png("res://screencap.png")


func start_game():
    $music/menu.playing = false
    $music/camion.playing = true
    Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
    var level = ADRENALINE
    if randf() >= 0.5:
        level = ADRENALINE
    else:
        level = DEFIBRILATEUR
    start_timeout(level)

func switch_scene(level=current_game):
#    Gets called in the middle of transition
    $Timer.paused = true
    if level_index == 0:
        scene_menu.visible = false
        $music/menu.playing = false
        $music/camion.playing = true
    
    if level_index >= level_number:
#        endgame
        
        $music/camion.playing = false
        $camera.end = true
        
        scene_defibrilateur.stop()
        scene_adrenaline.stop()
        current_game = END
        scene_end.visible = true
        scene_end.init()
    elif level == ADRENALINE:
        $music/transition.playing = false
        $music/adrenaline.playing = true
        scene_defibrilateur.stop()
        scene_adrenaline.init()
        current_game = ADRENALINE
    elif level == DEFIBRILATEUR:
        $music/transition.playing = false
        $music/defibrilateur.playing = true
        scene_adrenaline.stop()
        scene_defibrilateur.init(rand_range(15, 30), 0.0, rand_range(0.1, 0.3))
        current_game = DEFIBRILATEUR
    
    level_index +=1


func start_timeout(level):
    current_game = level
    if level_index >= level_number:
        level = END
    transition.transition = true
    transition.visible = true
    
    $music/defibrilateur.playing = false
    $music/adrenaline.playing = false
    $music/menu.playing = false
    
    $music/transition.playing = true

    transition.carton_adrenaline.visible = level == ADRENALINE
    transition.carton_defibrillateur.visible = level == DEFIBRILATEUR

    $Timer.paused = false
    $Timer.start()


func _on_menu_start_game():
    start_game()
    transition.visible = true
