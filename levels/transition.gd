extends Node2D


#enum transition_states { STOP, START, END }
#var transition_state = transition_states.STOP

var transition = false
export var transition_length = 5
export var max_scale_param = 3
var transition_timer = 0

export(Curve) var mask_curve = null
export(Curve) var alpha_curve = null

onready var carton_adrenaline = $ViewportContainer/Viewport/carton_adrenaline
onready var carton_defibrillateur = $ViewportContainer/Viewport/carton_defibrillateur


func _process(delta):

    # Debug
#    var input_enter = Input.is_action_pressed("ui_accept")
#    if input_enter:
#        transition = true

    if transition:
        transition_timer += delta / transition_length
        print(transition_timer)
        $transition.material.set_shader_param("mask_scale",
            mask_curve.interpolate(transition_timer) * max_scale_param)
        $transition.material.set_shader_param("mask_alpha_scale",
            alpha_curve.interpolate(transition_timer) * max_scale_param)
    
        if transition_timer > 1:
            transition = false
            transition_timer = 0
