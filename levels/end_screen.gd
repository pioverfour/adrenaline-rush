extends Node2D

var end_trigger = false
var end_timer = 0

func init():
    end_trigger = true

func _ready():
    pass # Replace with function body.


func _process(delta):
    if end_trigger:
        end_timer += delta
        if end_timer > 2.5:
            $fin.playing = true
            $end_screen.visible = true
            end_trigger = false
            set_process(false)
