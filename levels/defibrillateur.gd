extends Node2D

signal switch_scene
enum {MAIN, MENU, DEFIBRILATEUR, ADRENALINE, END}

enum mini_game_states { STOPPED, FIRSTSTART, STARTING, RESTART, STARTED, FINISHED, RESULT, VICTORY }
var mini_game = mini_game_states.FIRSTSTART

export var start_timer_value = 3
var start_timer = 0

enum gauge_states { LOW, CHARGED, OVERCHARGED }
var gauge_state = gauge_states.LOW
var previous_gauge_state = gauge_states.LOW

export var min_interval = 0.7
export var max_interval = 0.8
export var max_gauge = 20.0

var gauge_value = 0.0
enum dir_states { NONE, LEFT, RIGHT }
var dir = dir_states.NONE
var previous_dir = dir_states.NONE
var previous_action_dir = dir_states.NONE

export var action_press_timer = 0
var action_timer = 0

export var no_action_press_timer = 0.2
var no_action_timer = 0

export var end_total_timer = 0.5
export var delay_end_total_timer = 0.5
var end_timer = 0

var pitch = AudioServer.get_bus_effect(AudioServer.get_bus_index("charge"), 0)

var mouse_pos = Vector2(0, 0)
var previous_mouse_pos = Vector2(0, 0)
var delta_mouse_pos = Vector2(0, 0)

var previous_mouse_dir = 0
var mouse_dir = 0

var mouse_button_down = false

var brancard_pos = Vector2(0, 0)
var perso_pos = Vector2(0, 0)
var turbulences_brancard = Utils.Turbulence2D.new(0.03, 1.3)
var turbulences_perso = Utils.Turbulence2D.new(0.03, 1.3)

var victory = false
var victory_timer = 0

func init(charge_length=20, charge_timer=0, decrease_timer=0.2):
    set_process(true)
    mini_game = mini_game_states.STARTING
    start_timer = start_timer_value
    end_timer = end_total_timer
    gauge_value = 0
    victory_timer = 0
    victory = false
    
    brancard_pos = $patient.position
    perso_pos = $perso.position
    
    max_gauge = charge_length
    action_press_timer = charge_timer
    no_action_press_timer = decrease_timer
    
    visible = true
    
    $background/BG.visible = true
    $background/BG_shock.visible = false
    
    $perso/left.visible = true
    $perso/right.visible = false
    $perso/down.visible = false
    $perso/victory.visible = false
    
    $patient/repos.visible = true
    $patient/moyen.visible = false
    $patient/fort.visible = false
    $patient/victory.visible = false
    
    $fx_shock.visible = false
    $arcs.visible = false
    $text_shock.visible = false
    $perso/shock.visible = false
    
    $perso/sparkles.visible = true
    $perso/sparkles.emitting = false
    
    pitch.pitch_scale = 10


func stop():
    set_process(false)
    mini_game = mini_game_states.STOPPED
    visible = false
    $background/BG.visible = false
    $background/BG_shock.visible = false
    
    $perso/left.visible = false
    $perso/right.visible = false
    $perso/down.visible = false
    $perso/victory.visible = false
    
    $patient/repos.visible = false
    $patient/moyen.visible = false
    $patient/fort.visible = false
    $patient/victory.visible = false
    
    $fx_shock.visible = false
    $arcs.visible = false
    $text_shock.visible = false
    $perso/shock.visible = false
    
    $perso/sparkles.visible = false
    $perso/sparkles.emitting = false
    
    $perso/defibrillateur_start.playing = false
    $perso/defibrillateur_charge.playing = false
    $perso/defibrillateur_decharge.playing = false


func _ready():
    stop()
    init(5, 0.0, 1)


func _input(event):
    # Mouse in viewport coordinates
    if event is InputEventMouseMotion:
        mouse_pos = event.position
        delta_mouse_pos = previous_mouse_pos - mouse_pos
        previous_mouse_pos = mouse_pos
    if (event is InputEventMouseButton
            and (event.button_index == BUTTON_LEFT
                 or event.button_index == BUTTON_RIGHT)):
        mouse_button_down = not event.pressed


func _process(delta):
    var time = OS.get_ticks_msec() / 1000.0
    $patient.position = brancard_pos + turbulences_brancard.get_turbulence(time)
    $perso.position = perso_pos + turbulences_perso.get_turbulence(time)
    
    var input_left = Input.is_action_pressed("ui_left")
    var input_right = Input.is_action_pressed("ui_right")
    var input_down = Input.is_action_pressed("ui_down")
    
#    var input_enter = Input.is_action_pressed("ui_accept")
#    var input_space = Input.is_action_pressed("ui_select")
#
#    if input_enter:
#        stop()
#
#    if input_space:
##        stop()
#        init(rand_range(15, 30), 0.0, rand_range(0.1, 0.3))
##        init(max_gauge, action_press_timer, no_action_press_timer)

    if mini_game == mini_game_states.VICTORY:
        victory_timer += delta
        if victory_timer > 1:
            emit_signal("switch_scene")
            mini_game = mini_game_states.STOPPED

    if mini_game == mini_game_states.RESTART:
        init(max_gauge, action_press_timer, no_action_press_timer)
    
    if mini_game == mini_game_states.FIRSTSTART:
        start_timer -= delta * 2
        $ui/text.text = str(int(start_timer) + 1)
        if start_timer < 0:
            $ui/text.text = "Start!"
            mini_game = mini_game_states.STARTED
            $perso/defibrillateur_charge.autoplay = true
            $perso/defibrillateur_charge.playing = true
    elif mini_game == mini_game_states.STARTING:
        $ui/text.text = "Start!"
        mini_game = mini_game_states.STARTED
        $perso/defibrillateur_charge.autoplay = true
        $perso/defibrillateur_charge.playing = true
    elif mini_game == mini_game_states.STARTED:
        if (input_left
                or (delta_mouse_pos.y > 0
                and abs(delta_mouse_pos.y) > 60)):
            dir = dir_states.LEFT
        elif (input_right
                or (delta_mouse_pos.y < 0
                and abs(delta_mouse_pos.y) > 60)):
            dir = dir_states.RIGHT
        else:
            dir = dir_states.NONE
        
        if ((previous_dir == dir
                and dir != dir_states.NONE
                and previous_action_dir != dir)):
            action_timer += delta
        else:
            action_timer = 0
            no_action_timer += delta
        
        if action_timer > action_press_timer:
            if gauge_value < max_gauge:
                gauge_value += 1
#                print("gauge value: " + str(gauge_value))
            action_timer = 0
            previous_action_dir = dir
            if dir == dir_states.RIGHT:
                $perso/left.visible = false
                $perso/right.visible = true
            elif dir == dir_states.LEFT:
                $perso/left.visible = true
                $perso/right.visible = false

        if no_action_timer > no_action_press_timer:
            if gauge_value > 0:
                gauge_value -= 1
#                print("gauge value: " + str(gauge_value))
            no_action_timer = 0
        
            
        if gauge_value < min_interval * max_gauge:
            
            $ui/charge_state.text = "zzzzz...."
            gauge_state = gauge_states.LOW
        elif gauge_value > max_interval * max_gauge:
            gauge_state = gauge_states.OVERCHARGED
            $ui/charge_state.text = "Overcharged!!!!"
        else:
            gauge_state = gauge_states.CHARGED
            $ui/charge_state.text = "BZZZZ!"
            
        if previous_gauge_state != gauge_state:
            if gauge_state == gauge_states.LOW:
                $perso/sparkles.emitting = false
            elif gauge_state != gauge_states.LOW:
                $perso/sparkles.emitting = true
                
        previous_gauge_state = gauge_state
        
        if input_down or mouse_button_down:
            $ui/charge_state.text = "Finished"
            $perso/left.visible = false
            $perso/right.visible = false
            $perso/down.visible = true
            $perso/sparkles.emitting = false
            $perso/defibrillateur_start.playing = true
            $perso/defibrillateur_charge.playing = false
            
            mini_game = mini_game_states.FINISHED
            mouse_button_down = false
    
        pitch.pitch_scale = 10 + 6 * gauge_value / max_gauge
    
        previous_dir = dir
        previous_mouse_dir = mouse_dir
    
    elif (mini_game == mini_game_states.FINISHED
            or mini_game == mini_game_states.RESULT):
        if end_timer > 0:
            end_timer -= delta
        else:
            $perso/right.visible = false
            $perso/down.visible = false
            
            if mini_game == mini_game_states.FINISHED:
                if gauge_value < min_interval * max_gauge:
                    delay_end_total_timer = 0.5
                    $ui/resultat.text = "Restart with enter - too low"
                    $patient/repos.visible = false
                    $patient/moyen.visible = true
                    $patient/fort.visible = false
                    $perso/left.visible = true
                elif gauge_value > min_interval * max_gauge:
                    delay_end_total_timer = 1.0
                    $perso/defibrillateur_decharge.playing = true
                    $"../camera".shaking = true
                    $ui/resultat.text = ":)"
                    $patient/repos.visible = false
                    $patient/moyen.visible = false
                    $patient/fort.visible = true
                    $perso/left.visible = false
                    $perso/shock.visible = true
                    $fx_shock.visible = true
                    $arcs.visible = true
                    $text_shock.visible = true
                    $background/BG_shock.visible = true
                    victory = true
            
            mini_game = mini_game_states.RESULT
#			else:
#				$ui/resultat.text = ":)"
#				$patient/repos.visible = false
#				$patient/moyen.visible = true
#				$patient/fort.visible = false
            
            end_timer -= delta
            if end_timer < -delay_end_total_timer:
                $perso/defibrillateur_start.playing = false
                if victory:
                    mini_game = mini_game_states.VICTORY
                    $patient/repos.visible = false
                    $patient/moyen.visible = false
                    $patient/fort.visible = false
                    $patient/victory.visible = true
                    $perso/left.visible = false
                    $perso/right.visible = false
                    $perso/down.visible = false
                    $perso/victory.visible = true
                    $perso/victory_sound.playing = true
                else:
                    mini_game = mini_game_states.RESTART
                    $patient/repos.visible = true
                    $patient/moyen.visible = false
                    $patient/fort.visible = false
                    $patient/victory.visible = false
                    $perso/left.visible = true
                    $perso/right.visible = false
                    $perso/down.visible = false
                    $perso/victory.visible = false
                    
                $fx_shock.visible = false
                $arcs.visible = false
                $text_shock.visible = false
                $background/BG_shock.visible = false
                $perso/shock.visible = false
                

