extends TextureRect

export(bool) var flicker = false
export var flicker_speed = 0.5

var flicker_timer = 0.0
var flicker_state = false

func _ready():
    pass


func _process(delta):
    if flicker:
        flicker_timer += delta * flicker_speed
        visible = flicker_state
        
        if flicker_timer > 1:
            flicker_timer = 0
            flicker_state = !flicker_state
