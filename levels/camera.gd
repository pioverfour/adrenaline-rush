extends Camera2D

var low_shake_turbulence = Utils.Turbulence2D.new(0.1, 1)
var road_shake_turbulence = Utils.Turbulence2D.new(0.05, 5)
var screen_shake_turbulence = Utils.Turbulence2D.new(0.3, 40)

export(Curve) var curve = null
export(Curve) var road_curve = null

var road_shake = false
export var road_shaking_wait_length = 3
var road_shaking_wait_timer = 0
export var road_shaking_length = 0.3
var road_shaking_timer = 0

var shaking = false
export var shaking_length = 0.5
var shaking_timer = 0

var end = false


func _ready():
    road_shaking_wait_timer = rand_range(road_shaking_wait_length - 1, road_shaking_wait_length + 2)


func _process(delta):
    var time = OS.get_ticks_msec() / 1000.0
    if end:
        offset = Vector2(0, 0)
    else:
        offset = low_shake_turbulence.get_turbulence(time)
        
        if road_shaking_wait_timer < 0 and road_shake == false:
            road_shake = true
            
        if road_shake == true:
            offset += road_shake_turbulence.get_turbulence(time) * road_curve.interpolate(road_shaking_timer)
            road_shaking_timer += delta / road_shaking_length
            if road_shaking_timer > 1:
                road_shaking_timer = 0
                road_shaking_wait_timer = rand_range(road_shaking_wait_length - 1, road_shaking_wait_length + 2)
                road_shake = false
                
        road_shaking_wait_timer -= delta
        
        if shaking == true:
            offset += screen_shake_turbulence.get_turbulence(time) * curve.interpolate(shaking_timer)
            shaking_timer += delta / shaking_length
            if shaking_timer > 1:
                shaking = false
                shaking_timer = 0
