extends Node2D

signal switch_scene
enum {MAIN, MENU, DEFIBRILATEUR, ADRENALINE, END}

onready var curseur = $Curseur
onready var seringue = $Curseur/Seringue_turbulence/Seringue
onready var seringue_turbulence = $Curseur/Seringue_turbulence

onready var tete = $Patient/Tete
onready var corps = $Patient/Corps
onready var cible = $Patient/Corps/Cible
onready var corps_area = $Patient/Corps/Area2D
onready var seringue_area = $Curseur/Seringue_turbulence/Seringue/Area2D
onready var laser = $Curseur/Seringue_turbulence/Laser

onready var objets = $Objets
onready var masque = $Objets/objets_masque

var cooldown = Timer.new()

var sprite_plante = preload("res://Assets/Adrenaline/main_idle_fin.png")
var sprite_bras = preload("res://Assets/Adrenaline/main_idle.png")

var turbulence_bras = Utils.Turbulence2D.new(0.6, 1.0)
var turbulence_tete = Utils.Turbulence2D.new(0.05, 1.2)

var turbulences_objets = {}

enum {STOPPED, UP, MOVING_UP, MOVING_DOWN, DOWN, FINISHED, RESULT, VICTORY}
var state = UP
export var down_speed = 1.0
var time_up = 0.0


func _ready():
    Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
    for obj in objets.get_children():
        if obj.is_in_group("objets_rot"):
            turbulences_objets[obj] = Utils.Turbulence.new(0.1, 1.3)
        elif obj.is_in_group("objets_trans"):
            turbulences_objets[obj] = Utils.Turbulence2D.new(0.03, 1.3)

    add_child(cooldown)
    cooldown.connect("timeout", self, "reraise")


func init():
    set_process(true)
    seringue.stop()
    seringue.frame = 0
    state = MOVING_UP
    visible = true
    curseur.position = get_viewport().get_mouse_position()
    curseur.position.x = max(curseur.position.x, 300)


func stop():
    set_process(false)
    state = STOPPED
    visible = false


func _process(delta):
#    turbulences
    var time = OS.get_ticks_msec() / 1000.0
    tete.position = turbulence_tete.get_turbulence(time)
    for obj in objets.get_children():
        if obj.is_in_group("objets_rot"):
            obj.rotation_degrees = turbulences_objets[obj].get_turbulence(time)
        elif obj.is_in_group("objets_trans"):
            obj.get_children()[0].position = turbulences_objets[obj].get_turbulence(time)

    if state == UP:
        time_up += delta
        seringue_turbulence.position = turbulence_bras.get_turbulence(time_up)
    elif state == MOVING_DOWN:
        laser.visible = false
        var touche = corps_area.overlaps_area(seringue_area)
        if touche:
            seringue.play("stab")
            $audio_stab.playing = true
            if (cible.global_position - seringue.global_position).length() > 75:
#                bad!
                corps.frame = 2
                tete.frame = 3
                state = DOWN
                cooldown.paused = false
                cooldown.start()
            else:
#                good!
                tete.frame = randi() % 2 + 1
                corps.frame = 1
                state = VICTORY
                $Victory_timer.paused = false
                $Victory_timer.start()
        else:
#            miss
            seringue.frame = 0
            seringue.play("miss")
            $audio_miss.playing = true
            state = UP
    elif state == MOVING_UP:
        laser.visible = true
        tete.frame = 0
        corps.frame = 0
        state = UP


func _input(event):
    # Mouse in viewport coordinates
    if event is InputEventMouseMotion:
        if state == UP:
            curseur.position = event.position
            curseur.position.x = max(curseur.position.x, 300)

            var touche = corps_area.overlaps_area(seringue_area)
            if touche:
                laser.animation = "in"
                laser.z_index = 0
            else:
                laser.animation = "out"
                laser.z_index = -1
    elif (event is InputEventMouseButton
            and event.button_index == BUTTON_LEFT
            and event.pressed):
        if state == UP:
            state = MOVING_DOWN

func reraise():
    cooldown.paused = true
    state = MOVING_UP
    seringue.play("stab", true)
    $audio_stab.playing = true

func victory():
    $Victory_timer.paused = true
    emit_signal("switch_scene")

func _on_Seringue_animation_finished():
    if state == UP:
        laser.visible = true
