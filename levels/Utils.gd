extends Node


class Turbulence:
    var global_amplitude = 1.0
    var global_freq = 1.0
    var axes
    var amplitude = [200.0, 100.0, 50.0]
    var frequency = [1.0, 5.0, 10.0]
    var phase = [0.0, 0.0, 0.0]

    func _init(amp=1.0, freq=1.0):
        self.global_amplitude = amp
        self.global_freq = freq
        for i in range(3):
            self.amplitude[i] *= rand_range(0.8, 1.2)
            self.frequency[i] *= rand_range(0.8, 1.2)

    func get_turbulence(value):
        var turbulence = 0
        for i in range(3):
            turbulence += (cos(value
                               * self.frequency[i] 
                               * self.global_freq 
                               + self.phase[i])
                            * self.amplitude[i]
                            * self.global_amplitude)
        return turbulence


class Turbulence2D:
    var turbulence_x
    var turbulence_y

    func _init(amp=1.0, freq=1.0):
        self.turbulence_x = Turbulence.new(amp, freq)
        self.turbulence_y = Turbulence.new(amp, freq)

    func get_turbulence(value):
        return Vector2(turbulence_x.get_turbulence(value),
                       turbulence_y.get_turbulence(value))
