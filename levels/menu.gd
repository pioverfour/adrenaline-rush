extends Node2D


signal start_game

var start = false
var start_timer = 0

func _ready():
    pass


func _process(delta):
    if start:
        start_timer += delta
        if start_timer > 0.5:
            emit_signal("start_game")
            start = false
            set_process(false)


func _on_bouton_pressed():
    $button_visual/anim_start.current_animation = "start"
    start = true
